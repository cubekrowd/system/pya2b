# pya2b.py
# tiny converter for askyblock -> bskyblock player/island data for CK

import os
import json
import yaml
from os.path import join as jp
from tqdm import tqdm
import pickle
import uuid
from pprint import pprint
import tarfile
import io

def dummy_print(*a, **k):
    pass

debug = print


# CONFIGURATION
# this script will only be used once, so it's gonna be here

# player dirs in order of priority. on other servers this is likely just
# the players directory but CK configs are a mess
PLAYER_DIRS = ['players', 'players.backup', 'quarantine']

# cache file to store/load deserialized asb players in
ASB_PLAYER_CACHE = 'pya2b_asb_players.pickle'

# cache file to load bedrock coordinate locations
BEDROCK_FILE = 'pya2b_bedrock_coordinates.pickle'

# list of ((x0, z0), (x1, z1)) defining corners of rectangles within which
# we should not care about bedrock blocks. In CK, we do not provide players
# with bedrock, so this never happens except in rare cases.
IGNORE_AREAS = [
        ((5290, 310), (5310, 330)), # Foorack's island
        ((-50, -50), (50, 50)) # Spawn
]

# adding this to an islands x or z coordinate will get you another valid island
# location
ISLAND_RANGE = 83

# directory containing challenges *manually written* or transferred to new
# BentoBox format.
CHALLENGES_DIR = os.path.join('plugins', 'BentoBox', 'database', 'Challenge')

# File to output results to
OUTPUT_FILE = 'pya2b_results.tar.gz'

def make_bsb_island(key, owner, members, source=None):
    members = dict(zip(members, (500,)*len(members)))
    if owner:
        members[owner] = 1000
    bsb_island = {
            'deleted': False,
            'uniqueId': 'pya2b_skyblock_i_' + str(uuid.uuid4()),
            'center': list(key),
            'range': ISLAND_RANGE,
            'protectionRange': 75,
            'maxEverProtectionRange': 75,
            'world': key[0],
            'gameMode': 'BSkyBlock',
            'createdDate': 0,
            'updatedDate': 0,
            'owner': owner,
            'members': members,
            'spawn': False,
            'purgeProtected': False,
            'flags': dict(LOCK=0, ANIMAL_SPAWN=500, MONSTER_SPAWN=500,
                CONTAINER=500, PVP_OVERWORLD=-1, ITEM_DROP=500,
                PRESSURE_PLATE=500, ITEM_PICKUP=500,
                NETHER_PORTAL=500, BED=500, DOOR=500),
            'history': [],
            'levelHandicap': 0,
            'spawnPoint': {},
            'doNotLoad': False,
            'cooldowns': {},
            '__pya2b_source': source
    }
    if not owner:
        del bsb_island['owner'] # leave it unowned...
    return bsb_island


def make_bsb_player(uuid, name, deaths, homes, source=None):
    if homes:
        out_homes = []
        for id, home in homes.items():
            home = home.split(':')
            if home[0] == 'skyblock':
                for i in range(1, len(home)):
                    home[i] = float(home[i]) + 0.5

                home = home[:4]
                home.append(0.)
                home.append(0.)
                out_homes.append([home, 1])
    else:
        out_homes = {}

    if len(out_homes) > 1:
        print(f'{uuid}: WARNING: more than one home found, using the first '
              f'overworld one!')
    if len(out_homes) > 0:
        out_homes = [out_homes[0]]

    return dict(homeLocations=out_homes, uniqueId=uuid, playerName=name,
                deaths=dict(skyblock=deaths), pendingKicks=[], resets={},
                flagsDisplayMode='BASIC', __pya2b_source=source,
                locale='en-US')


def make_tar_member(f, path, data):
    info = tarfile.TarInfo(name=path)
    with io.BytesIO() as b:
        b.write(json.dumps(data).encode('utf8'))
        info.size = b.tell()
        b.seek(0)
        f.addfile(info, b)



askyblock_dir = jp('plugins', 'ASkyBlock')

if os.path.isfile(ASB_PLAYER_CACHE):
    print(f'Previous cache found in {ASB_PLAYER_CACHE}, using that...')
    with open(ASB_PLAYER_CACHE, 'rb') as f:
        asb_players = pickle.load(f)
else:
    asb_players = {}
    for player_dir in PLAYER_DIRS:
        asb_players[player_dir] = {}
        print('Reading players:', player_dir)
        with os.scandir(jp(askyblock_dir, player_dir)) as it:
            for player_file in tqdm(it):
                # print(player_file.path)
                if not player_file.is_file():
                    continue
                with open(player_file) as f:
                    loaded_player = yaml.safe_load(f)
                    if loaded_player is None:
                        print(f'WARNING: loading player file {player_file} returned None. '
                              f'Player file is of size {player_file.stat().st_size} bytes.')
                    asb_players[player_dir][os.path.splitext(player_file.name)[0]] = loaded_player

    print(f'Writing player cache to {ASB_PLAYER_CACHE}...')
    with open(ASB_PLAYER_CACHE, 'wb') as f:
        pickle.dump(asb_players, f)

######

# Read challenges first so we can write challenge player data
print(f'Reading challenges from {CHALLENGES_DIR}')
challenges = {}
with os.scandir(CHALLENGES_DIR) as it:
    for cfile in tqdm(it):
        if not cfile.is_file():
            continue
        with open(cfile) as f:
            challenge = json.load(f)
            challenge_name = os.path.splitext(cfile.name)[0].lower()
            challenge_name = challenge_name.replace('bskyblock', 'BSkyBlock')
            challenge['uniqueId'] = challenge_name
            challenges[challenge_name] = challenge
print(f'Loaded {len(challenges)} challenges.')

# Convert players
islands = {}
island_owners_from_teams = {}
players = {}
known_players = set()
levels = {}
challenge_completions = {}
unknown_challenges = set()

for i, (player_dir, dirplayers) in enumerate(asb_players.items()):
    print(f'Processing directory "{player_dir}" ({i+1}/{len(asb_players)})')
    for j, (puuid, playerdata) in enumerate(dirplayers.items()):
        # print(f'{player_dir}: processing {puuid} ({j+1}/{len(asb_players)})')

        if playerdata is None:
            print(f'{player_dir}/{puuid}: WARNING: playerdata is null, skipping.')
            continue

        if puuid in players:
            print(f'{player_dir}/{puuid}: Found player twice, skipping.')
        else:
            players[puuid] = make_bsb_player(puuid, playerdata['playerName'],
                                             playerdata.get('deaths', 0),
                                             playerdata.get('homeLocations', None),
                                             source=player_dir)
        known_players.add(puuid)

        if not playerdata['hasIsland']:
            if playerdata['hasTeam']:
                # We may be able to reconstruct island information and owner
                # from this file.
                key = playerdata['teamIslandLocation'].split(':')
                key = tuple([key[0]] + [int(x) for x in key[1:4]] + [0, 0])

                leader = playerdata['teamLeader']
                known_players.add(leader)

                if key in island_owners_from_teams:
                    existing = island_owners_from_teams[key]
                    if existing[0] != leader:
                        print(f'{player_dir}/{puuid}: WARNING: trying to '
                              f'reconstruct island {key}, but its existing '
                              f'owner {existing[0]} is different from '
                              f'proposed owner {leader} - skipping.')
                    else:
                        existing[1].add(puuid)
                        print(f'{player_dir}/{puuid}: added to island '
                              f'reconstruction at {key}, owner {leader}.')
                else:
                    island_owners_from_teams[key] = (leader, {leader, puuid})
                    print(f'{player_dir}/{puuid}: made new island reconstruction '
                          f'at {key}, with owner {leader}.')
            else:
                print(f'{player_dir}/{puuid}: skipping. has no island and no team')
            continue

        if 'islandLevel' in playerdata and puuid not in levels:
            levels[puuid] = dict(uniqueId=puuid, levels={'skyblock': playerdata['islandLevel']},
                                 initialLevel={})

        if 'challenges' in playerdata and puuid not in challenge_completions:
            pchallenges = playerdata['challenges']
            challenge_status = pchallenges['status']
            challenge_status = {'BSkyBlock_' + k: int(v) for k, v in challenge_status.items() if v}
            if 'times' in pchallenges:
                for challenge, count in pchallenges['times'].items():
                    if count > 0:
                        challenge_status['BSkyBlock_' + challenge] = count

            challenge_times = {k: 0 for k, v in challenge_status.items()}
            if 'timestamp' in pchallenges:
                for challenge, timestamp in pchallenges['timestamp'].items():
                    if timestamp != 0:
                        challenge_times['BSkyBlock_' + challenge] = timestamp

            # check for unknown challenges
            unknown_challenges.update(challenge_status.keys() - challenges.keys())
            for uchallenge in unknown_challenges:
                challenge_status.pop(uchallenge, None)
                challenge_times.pop(uchallenge, None)
            challenge_completions[puuid] = dict(uniqueId=puuid, challengeStatus=challenge_status,
                                                challengeTimestamp=challenge_times)

        key = playerdata['islandLocation'].split(':')
        key = tuple([key[0]] + [int(x) for x in key[1:4]] + [0, 0])

        if key in islands:
            other_island = islands[key]
            if other_island['owner'] == puuid:
                print(f'{player_dir}/{puuid}: duplicate, but same owner - skipping...')
                continue
            print(f'{player_dir}/{puuid}: WARNING: island {key} owned by {islands[key]["owner"]} from {islands[key]["__pya2b_source"]}, skipping this duplicate...')
            continue

        print(f'{player_dir}/{puuid}: successfully making island {key}!')
        islands[key] = make_bsb_island(key, puuid, playerdata['members'], source=player_dir)
        known_players.update(playerdata['members'])

print(f'Success. We have {len(islands)} islands total.')

# Any unknown challenges?
print(f'Player files had {len(unknown_challenges)} unknown challenges:')
pprint(unknown_challenges)

# Are any extra islands revealed by looking at team members' player files?
missing_teams = island_owners_from_teams.keys() - islands.keys()
print(f'{len(missing_teams)} islands were found by inspecting team member files. Reconstructing them now...')
for key in missing_teams:
    missing_island = island_owners_from_teams[key]
    islands[key] = make_bsb_island(key, missing_island[0], missing_island[1], source='team members')

# Do we have any missing player files?
missing_players = known_players - players.keys()
print(f'{len(missing_players)} players were found by inspecting other player '
      f'files. Substituting default values for these players...')
for key in missing_players:
    missing_player = make_bsb_player(key, '', 0, None, source='other player files')

# Check for duplicate leaders or players
known_players = set()
islands_with_duplicates = 0
for key, island in islands.items():
    duplicates = known_players & island['members'].keys()
    if len(duplicates) > 0:
        islands_with_duplicates += 1
        print(f'WARNING: duplicates found in consistency check: {duplicates}')
    known_players |= duplicates
if islands_with_duplicates:
    print(f'WARNING: {islands_with_duplicates} islands with duplicates found')

# Search for bedrock in the world
if os.path.isfile(BEDROCK_FILE):
    print('Checking for unaccounted bedrock blocks...')
    with open(BEDROCK_FILE, 'rb') as f:
        bedrock_coords = pickle.load(f)
    ignore_bedrock = set()
    for c in bedrock_coords:
        for ign in IGNORE_AREAS:
            if c[1] >= ign[0][0] and c[1] <= ign[1][0] and c[3] >= ign[0][1] and c[3] <= ign[1][1]:
                ignore_bedrock.add(c)
                break
    
    bedrock_coords -= ignore_bedrock
    unaccounted = bedrock_coords - islands.keys()
    empty_islands = islands.keys() - bedrock_coords
    print(f'There are {len(bedrock_coords)} bedrock blocks in the world, '
          f'{len(unaccounted)} of which are not associated with islands.')
    print(f'{len(empty_islands)} islands are missing their bedrock blocks!')
    print(f'{len(ignore_bedrock)} bedrock blocks were ignored by configuration.')

    unaligned_unaccounted = {c for c in unaccounted if c[1] % ISLAND_RANGE != 0 or c[3] % ISLAND_RANGE != 0}
    print(f'{len(unaligned_unaccounted)} of these unassociated bedrock blocks are not aligned:')
    print(unaligned_unaccounted)

    print('Creating dummy islands for unassociated but aligned bedrock blocks...')
    for c in unaccounted:
        if c in unaligned_unaccounted: continue
        islands[c] = make_bsb_island(c, None, {}, source='bedrock')
        
print(f'Done processing {len(islands)} islands and {len(players)} players.')


# Read warps
print('Loading warps...')
with open('plugins/ASkyBlock/warps.yml') as f:
    asb_warps = yaml.safe_load(f)['warps']

warps = []
for puuid, warp in tqdm(asb_warps.items()):
    if puuid not in players:
        print(f'WARNING: skipping warp for unknown player {puuid}')
        continue

    warp = warp.split(':')[:4]
    warps.append([[warp[0], float(warp[1]) + 0.5, float(warp[2]),
                   float(warp[3]) + 0.5, 0., 0.], puuid])

print(f'Success. {len(warps)} warps loaded.')
warps = dict(uniqueId='warps', warpSigns=warps)

# Read top ten
print('Loading top ten...')
with open('plugins/ASkyBlock/topten.yml') as f:
    asb_topten = yaml.safe_load(f)['topten']
topten = dict(uniqueId='skyblock', topTen=asb_topten)

print(f'Opening (overwriting!) tarfile {OUTPUT_FILE}')
os.remove(OUTPUT_FILE)
with tarfile.open(OUTPUT_FILE, 'w|gz') as f:
    print('Writing islands...')
    for _, island in tqdm(islands.items()):
        make_tar_member(f, os.path.join('Island', island['uniqueId'] + '.json'), island)
    print('Writing players...')
    for _, player in tqdm(players.items()):
        make_tar_member(f, os.path.join('Players', player['uniqueId'] + '.json'), player)
    print('Writing warps...')
    make_tar_member(f, 'WarpsData/warps.json', warps)
    print('Writing levels...')
    for _, leveldata in tqdm(levels.items()):
        make_tar_member(f, os.path.join('LevelsData', leveldata['uniqueId'] + '.json'), leveldata)
    print('Writing top ten...')
    make_tar_member(f, 'TopTenData/skyblock.json', topten)
    print('Writing challenges...')
    for _, challenge in tqdm(challenges.items()):
        make_tar_member(f, os.path.join('Challenge', challenge['uniqueId'] + '.json'), challenge)
    print('Writing challenge completions...')
    for _, challenge_completion in tqdm(challenge_completions.items()):
        make_tar_member(f, os.path.join('ChallengesPlayerData', challenge_completion['uniqueId'] + '.json'), challenge_completion)
print(f'New BSB database files written to {OUTPUT_FILE}, have fun!')
