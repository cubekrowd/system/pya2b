# pya2b

A quick-n-dirty script for converting CubeKrowd's ASkyBlock files to
BSkyBlock files, with consistency checking and island reconstruction.

**This script is provided as a reference. We provide zero support! Use at your
own risk.**

## Features
- Converts islands, players, levels, top ten, warps, homes to BSkyBlock format.
  (Note that it does not convert challenges! We decided to rewrite these
   manually!)
- Island reconstruction from team members' playerfiles, and bedrock positions.

## Prerequisites

- Create a python 3.6+ virtualenv
- `pip install pyyaml tqdm`
- For bedrock finding, grab [this NBT package](https://github.com/twoolie/NBT)
  and use `python setup.py install`. I also had to make one modification to
  `iter_chunks` to allow it to not fail in the middle when reading one corrupt
  chunk in the world.

