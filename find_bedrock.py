import nbt, pickle
import sys

BEDROCK = 7
world = nbt.world.AnvilWorldFolder('skyblock')
coords = set()

if not world.nonempty():
    print('world dir empty!')
    exit(1)

for chunk in world.iter_nbt():
    level = chunk['Level']

    sections = level['Sections']
    if len(sections) == 0:
        continue

    off_x = level['xPos'].value*16
    off_z = level['zPos'].value*16
    for section in sections:
        off_y = section['Y'].value * 16
        # indices in BSkyBlock format (world, x, y, z, 0, 0)
        bedrock_indices = [('skyblock', (i & 0xF) + off_x, (i >> 8) + off_y, ((i >> 4) & 0xF) + off_z, 0, 0) for i in range(4096) if section['Blocks'][i] == BEDROCK]
        if len(bedrock_indices) == 0: continue
        coords.update(bedrock_indices)
        print(bedrock_indices, file=sys.stderr)

with open('pya2b_bedrock_coordinates.pickle', 'wb') as f:
    pickle.dump(coords, f)

